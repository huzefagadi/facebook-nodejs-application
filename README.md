facebook-login-node-express
Facebook login Script using Node and Express

Download the code and run <code>npm install</code> then <code>node app.js</code>

Access the app at localhost:3000.

-- Table structure for table user_info
CREATE TABLE user_info ( user_id varchar(30) NOT NULL, user_name varchar(30) NOT NULL, coins int(11) NOT NULL DEFAULT '0', accessToken varchar(300) DEFAULT NULL, firstname varchar(200) NOT NULL, lastname varchar(200) NOT NULL, email varchar(200) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

you need to change the database config in file config.js
and will have to change the port/username/password/database name as per your needs

load the coins
http://localhost:3000/loadUserCoins

