const mysql = require('mysql');
const config = require('../configuration/config');
const squel = require('squel');
const squelMySql = squel.useFlavour('mysql');

// create connection
const connection = mysql.createConnection({
  host: config.host,
  port: config.port,
  user: config.username,
  password: config.password,
  database: config.database,
});

if (config.use_database === 'true') {
  connection.connect();
}

/**
 * Execute a query and returns a Promise
 * @param {*} text
 * @param {*} values
 */
function query(text, values) {
  return new Promise((resolve, reject) => {
    connection.query(text, values, function(error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

/**
 * Execute a query and return first row
 * @param {*} text
 * @param {*} values
 */
function queryUnique(text, values) {
  return query(text, values).then(rows => {
    if (rows.length === 0) return null;
    return rows[0];
  });
}

/**
 * Helper function to insert one or many elements
 * @param {*} tableName
 * @param {*} values
 */
function insert(tableName, values) {
  let request;

  if (values instanceof Array) {
    // if array is empty, don't go to DB
    if (values.length === 0) return null;
    request = squelMySql
      .insert()
      .into(tableName)
      .setFieldsRows(values)
      .toParam();
  } else {
    request = squelMySql
      .insert()
      .into(tableName)
      .setFields(values)
      .toParam();
  }

  return query(request.text, request.values).then(rows => {
    if (values instanceof Array) return rows;
    return rows[0];
  });
}

/**
 * Update rows in DB
 * @param {*} tableName
 * @param {*} condition
 * @param {*} values
 */
function update(tableName, condition, values) {
  const request = squelMySql
    .update()
    .table(tableName)
    .setFields(values);
  for (let i = 0; i <= condition.length - 2; i += 2) {
    request.where(condition[i], condition[i + 1]);
  }
  request.toParam();
  const result = query(request.toString());
  return result;
}

/**
 * Update one row
 * @param {*} tableName
 * @param {*} condition
 * @param {*} values
 */
function updateOne(tableName, condition, values) {
  return update(tableName, condition, values).then(rows => {
    if (rows.length === 0) return null;
    return rows[0];
  });
}

/**
 * Delete rows in DB
 * @param {*} tableName
 * @param {*} condition
 */
function remove(tableName, condition) {
  const request = squelMySql.delete().from(tableName);
  for (let i = 0; i <= condition.length - 2; i += 2) {
    request.where(condition[i], condition[i + 1]);
  }
  const result = query(request.toString());
  return result;
}

/**
 * Delete one row
 * @param {*} tableName
 * @param {*} condition
 */
function removeById(tableName, condition) {
  return remove(tableName, condition).then(rows => {
    if (rows.length === 0) return null;
    return rows[0];
  });
}

module.exports = {
  query,
  queryUnique,
  insert,
  update,
  updateOne,
  remove,
  removeById,
};
