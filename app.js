const express = require("express");
const passport = require("passport");
const util = require("util");
const FacebookStrategy = require("passport-facebook").Strategy;
//FacebookTokenStrategy = require('passport-facebook-token');
const session = require("express-session");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const config = require("./configuration/config");
const morgan = require("morgan");
const app = express();

// controllers
const userController = require("./api/user/user.controller");

// passport session setup
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

// passport facebook strategy setup
passport.use(
  new FacebookStrategy(
    {
      clientID: config.facebook_api_key,
      clientSecret: config.facebook_api_secret,
      callbackURL: config.callback_url,
      profileFields: ["id", "emails", "name"]
    },
    function(accessToken, refreshToken, profile, done) {
      // set access token
      profile.accessToken = accessToken;
      if (config.use_database === "true") {
        // handle successful login
        return userController
          .loginSuccess(profile)
          .then(() => done(null, profile));
      }
      return done(null, profile);
    }
  )
);

// set views
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// setup logging
app.use(morgan("combined"));

// setup cookie parsing
app.use(cookieParser());

// setup body parsing
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// setup session and intialize passport
app.use(session({ secret: "keyboard cat", key: "sid" }));
app.use(passport.initialize());
app.use(passport.session());

// set public directory
app.use(express.static(__dirname + "/public"));

app.get("/", function(req, res) {
  res.render("index", { user: req.user });
});

/**
 *
 * Ensure that user is authenticated
 *
 */
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect("/");
}

// account route
app.get(
  // route
  "/account",
  // ensure user is authenticated
  ensureAuthenticated,
  // render index page
  function(req, res) {
    res.render("account", { user: req.user });
  }
);

// load user coins route
app.get(
  // route
  "/loadUserCoins",
  // ensure user is authenticated
  ensureAuthenticated,
  // function to fetch user coins
  function(req, res) {
    if (config.use_database === "true") {
      userController
        .findById(req.user.id)
        .then(user => {
          // send user coins in response
          res.send(200, { coins: user.coins });
        })
        .catch(err => {
          throw err;
        });
    }
  }
);

// update user coins route
app.get(
  // route
  "/updateUserCoins",
  // ensure user is authenticated
  ensureAuthenticated,
  // function to update user coins
  function(req, res) {
    // get user id and coins from request query params
    const { userId, coins } = req.query;

    if (config.use_database === "true") {
      // update coins
      userController
        .updateCoins(userId, coins)
        .then(() => {
          res.send(200, { status: "success" });
        })
        .catch(err => {
          throw err;
        });
    }
  }
);

// facebook login route
app.get(
  // route
  "/auth/facebook",
  // passport auth
  passport.authenticate("facebook", { scope: "email", session: false })
);

// facebook auth callback route
app.get(
  // route
  "/auth/facebook/callback",
  // passport authenticate
  passport.authenticate("facebook", {
    // redirect to home on success
    successRedirect: "/",
    // redirect to home on failure
    failureRedirect: "/login"
  })
);

// logout route
app.get(
  // route
  "/logout",
  // function to logout
  function(req, res) {
    req.logout();
    res.redirect("/");
  }
);

// listen on port 3003
app.listen(3000);
