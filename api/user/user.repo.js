const sql = require('./user.sql');
const db = require('../../service/db');

function findById(id) {
  return db.queryUnique(sql.findById, [id]);
}

function insert(user) {
  return db.insert('user_info', user);
}

function update(id, values) {
  return db.update('user_info', ['user_id = ?', id], values);
}

module.exports = {
  findById,
  insert,
  update,
};
