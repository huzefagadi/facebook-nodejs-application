const userRepo = require('./user.repo');

function findById(id) {
  return userRepo.findById(id);
}

function insert(user) {
  return userRepo.insert(user);
}

function updateCoins(id, coins) {
  return userRepo.update(id, { coins });
}

function updateAccessToken(id, accessToken) {
  return userRepo.update(id, { accessToken });
}

function loginSuccess(profile) {
  return findById(profile.id)
    .then(user => {
      if (user) {
        // user already exists in database
        updateAccessToken(user.user_id, profile.accessToken);
      } else {
        // there is no such user, should insert
        const {
          id,
          username,
          accessToken,
          name: { givenName: firstname, familyName: lastname },
        } = profile;
        user = {
          user_id: id,
          username,
          accessToken,
          firstname,
          lastname,
          email: profile.emails[0] ? profile.emails[0].value : '',
        };
        insert(user);
      }
      return user;
    })
    .catch(err => {
      throw err;
    });
}

module.exports = {
  findById,
  insert,
  updateAccessToken,
  updateCoins,
  loginSuccess,
};
